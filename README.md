Autor del código del reloj: __Rubén Verdute Ávila (https://plus.google.com/+RubenVerduteAvila)__

Al que he añadido el logotipo de GNU

Puedes verlo en acción en la siguiente dirección: https://victorhck.gitlab.io/GNU-Linux_clock/

Aquí puedes ver el resultado:

![](reloj.png)

Bajo licencia [MIT](https://opensource.org/licenses/MIT)